package main.java.com.builder_pattern.demo.BuilderPatter;

public class CarBuilder {
    private Car car;

    public CarBuilder() {
        this.car = new Car();
    }
    public CarBuilder withMake(String make) {
        car.setMake(make);
        return this;
    }
    public CarBuilder withModel(String model) {
        car.setModel(model);
        return this;
    }

    public CarBuilder withYear(int year) {
        car.setYear(year);
        return this;
    }
    public Car build(){
        return car;
    }
}