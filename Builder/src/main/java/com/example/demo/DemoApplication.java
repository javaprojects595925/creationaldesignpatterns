package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import main.java.com.builder_pattern.demo.BuilderPatter.Car;
import main.java.com.builder_pattern.demo.BuilderPatter.CarBuilder;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@SpringBootApplication
public class DemoApplication {
	@RequestMapping("/")
	String home() {
		Car car = new CarBuilder()
				.withMake("Toyota")
				.withModel("Corolla")
				.withYear(2022)
				.build();

		System.out.println(car.toString());
		return "Pattern Builder!";
	}

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
