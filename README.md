# CreationalDesignPatterns



## Content:

- [Abstract Factory](./AbstractFactory)|  [video](https://youtu.be/C_YxxGzEV-s) ✔️
- [Builder](./Builder) [video](https://www.youtube.com/watch?v=R1GdULHnm0U) ✔️
- [Prototype](./prototype) | [video](https://youtu.be/OkCQxRrgdpQ) o
