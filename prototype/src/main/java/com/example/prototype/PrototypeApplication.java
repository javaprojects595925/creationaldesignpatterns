package com.example.prototype;

import java.util.ArrayList;
import java.util.List;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import main.java.com.example.prototype.Shapes.Circle;
import main.java.com.example.prototype.Shapes.Rectangle;
import main.java.com.example.prototype.Shapes.Shape;

@SpringBootApplication
public class PrototypeApplication {
	public static void copyShapes(List<Shape> shapes,List<Shape> shapesCopy) {
		for (Shape shape : shapes) {
            shapesCopy.add(shape.clone());
        }
	}

    private static void compare(List<Shape> shapes, List<Shape> shapesCopy) {
        for (int i = 0; i < shapes.size(); i++) {
            if (shapes.get(i) != shapesCopy.get(i)) {
                System.out.print(i + ": Shapes are different objects");
                if (shapes.get(i).equals(shapesCopy.get(i))) {
                    System.out.println(". And they are identical");
                } else {
                    System.out.println(". But they are not identical");
                }
            } else {
                System.out.println(i + ": Shape objects are the same");
            }
        }
    }
	public static void main(String[] args) {
		SpringApplication.run(PrototypeApplication.class, args);
		List<Shape> shapes = new ArrayList<>();
		List<Shape> shapesCopy = new ArrayList<>();
        Circle circle = new Circle();
        circle.x = 10;
        circle.y = 20;
        circle.radius = 15;
        circle.color = "red";
        shapes.add(circle);

        Circle anotherCircle = (Circle) circle.clone();
		anotherCircle.color = "pink";
        shapes.add(anotherCircle);

        Rectangle rectangle = new Rectangle();
        rectangle.width = 10;
        rectangle.height = 20;
        rectangle.color = "blue";
        shapes.add(rectangle);

		Rectangle anotherRectangle = (Rectangle) rectangle.clone();
		anotherRectangle.color = "bluepink";
        shapes.add(anotherRectangle);

		// Clonar
		copyShapes(shapes,shapesCopy);
		compare(shapes,shapesCopy);
	}

}
